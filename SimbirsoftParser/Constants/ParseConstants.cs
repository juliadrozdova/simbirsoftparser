﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimbirsoftParser.Constants
{
    public static class ParseConstants
    {
        public static string ScriptsSection = "script";

        public static string StyleSection = "style";

        public static char[] SplitChars = { ' ', ',', '.', '!', '?', '"', '«', '»', '#', '&', '—', '$', ';', ':', '[', ']', '(', ')', '\n', '\r', '\t' };

        public static string[] FilteredWords = { "-", " - ", "/" };
    }
}

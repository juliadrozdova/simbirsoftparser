﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using System.Diagnostics;
using SimbirsoftParser.Constants;
using SimbirsoftParser.Models;

namespace SimbirsoftParser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            var htmlWeb = new HtmlWeb();
            var sb = new StringBuilder();

            var htmlDocument = htmlWeb.Load("https://www.simbirsoft.com");
            var rootNode = htmlDocument.DocumentNode;
            
            foreach (var node in rootNode.Descendants().Where(n =>
                n.NodeType == HtmlNodeType.Text &&
                n.ParentNode.Name != ParseConstants.ScriptsSection &&
                n.ParentNode.Name != ParseConstants.StyleSection))
            {
                if (!node.HasChildNodes)
                {
                    string text = node.InnerText;
                    if (!string.IsNullOrEmpty(text))
                        sb.AppendLine(text.Trim());
                }
            }

            var splitedStrings = sb.ToString()
                .ToUpper()
                .Split(ParseConstants.SplitChars, StringSplitOptions.RemoveEmptyEntries);

            var groupedWords = splitedStrings.GroupBy(x => x)
                .Where(x => !ParseConstants.FilteredWords.Contains(x.Key))
                .Select(x => new GroupedWord() 
                { 
                    Word = x.Key,
                    Count = x.Count()
                })
                .OrderByDescending(x => x.Count);
            
            foreach (var item in groupedWords)
            {
                listBox1.Items.Add($"{item.Word} - {item.Count}");
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
